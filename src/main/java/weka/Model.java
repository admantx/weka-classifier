package weka;

import weka.classifiers.trees.J48;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.gui.treevisualizer.PlaceNode2;
import weka.gui.treevisualizer.TreeVisualizer;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class Model {

    private J48 tree;
    private Instances data;

    public Model(boolean load, String modelFile, String dataFile){

        // load data
        try {
            data = new Instances(new BufferedReader(new FileReader(dataFile)));
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        // load model
        if (load && !modelFile.equals("")){
            try {
                tree = (J48) weka.core.SerializationHelper.read(modelFile);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        // train model
        else{
            tree = new J48();
            String[] options = new String[1];
            options[0] = "-U";

            try {
                tree.setOptions(options);

                assert false;
                data.setClassIndex(data.numAttributes() - 1);

                tree.buildClassifier(data);
                // change this parameter - SAVE THE MODEL
                // don't put as parameter to protect production environment
                // better to know manually what is this and where save it.
                weka.core.SerializationHelper.write("/Users/resposti/j48.model", tree);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void drawModel() throws Exception {

        TreeVisualizer Tv = new TreeVisualizer(null, tree.graph(), new PlaceNode2());
        JFrame frame = new javax.swing.JFrame("The Tree");
        frame.setSize(1500,1000);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(Tv);
        frame.setVisible(true);
        Tv.fitToScreen();
        Tv.setAutoscrolls(true);


    }

    public String classify(String[] parameters) throws Exception {

        data.setClassIndex(data.numAttributes() - 1);

        Instance instance = new DenseInstance(data.numAttributes());
        instance.setDataset(data);

        for(int i=0; i<data.numAttributes()-1; i++) {

            if(!data.attribute(i).isNominal())
                instance.setValue(data.attribute(data.attribute(i).name()), Double.parseDouble(parameters[i]));
            else
                instance.setValue(data.attribute(data.attribute(i).name()), parameters[i]);
        }

        return data.classAttribute().value((int) tree.classifyInstance(instance));

    }
}
